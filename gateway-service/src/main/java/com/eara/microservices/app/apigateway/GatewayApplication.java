package com.eara.microservices.app.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * This class represents the Spring Cloud Gateway used in
 * the application.
 *
 * Spring Cloud Gateway provides three basic components used for configuration: routes, predicates and filters.
 *
 * Route is the basic building block of the gateway. It contains destination URI and list of defined predicates
 * and filters.
 *
 * Predicate is responsible for matching on anything from the incoming HTTP request, such as headers or parameters.
 *
 * Filter may modify request and response before and after sending it to downstream services. All these components
 * may be set using configuration properties.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@SpringBootApplication
@EnableDiscoveryClient
public class GatewayApplication {

    public static void main(String[] args) {

        SpringApplication.run(GatewayApplication.class, args);
    }
}
