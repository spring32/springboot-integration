package com.eara.microservices.app.student.application.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api")
public class StudentQueryController {

//    private final StudentQueryFacade studentQueryService;

    @GetMapping("/v1/message")
    public String dummyGet() {
        return "Student Service Dummy GET";
    }

    /**
     * Get the student with that id.
     * @param studentId
     * @return CompletableFuture<StudentDTO>

    @ApiOperation(value = "Retrieve an student with an specific id",
            httpMethod = "GET",
            consumes = "application/json",
            produces = "application/json",
            response = CompletableFuture.class)
    @GetMapping("/v1/{studentId}")
    public CompletableFuture<StudentDTO> findById(@PathVariable("studentId") Long studentId) {

        return this.studentQueryService.findById(new StudentIdentifier(studentId));
    }*/

    /**
     * Get all the events for an specific student ID.
     * @return List<Object>

    @ApiOperation(value = "Retrieve all the students",
            httpMethod = "GET",
            consumes = "application/json",
            produces = "application/json",
            response = List.class)
    @GetMapping("/v1/{studentId}/events")
    public List<Object> listEventsForStudent(@PathVariable("studentId") Long studentId) {

        return this.studentQueryService
                .listEventsForStudent(new StudentIdentifier(studentId));
    }*/
}

