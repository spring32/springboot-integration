package com.eara.microservices.app.student.core.port.incoming;

import java.util.concurrent.CompletableFuture;

public interface RemoveStudent {
    CompletableFuture<Boolean> handle(Long studentId);
}
