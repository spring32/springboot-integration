package com.eara.microservices.app.student.application.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StudentController {

    @RequestMapping("/")
    @ResponseBody
    public String testFindAll() {
        return "FindAll testing";
    }
}
