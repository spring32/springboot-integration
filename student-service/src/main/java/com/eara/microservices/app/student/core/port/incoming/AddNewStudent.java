package com.eara.microservices.app.student.core.port.incoming;

import com.eara.microservices.app.student.application.exception.InvalidEmailException;
import com.eara.microservices.app.student.infrastructure.mapper.StudentDTO;

import java.util.concurrent.CompletableFuture;

public interface AddNewStudent {
    CompletableFuture<StudentDTO> handle(StudentDTO studentDTO) throws InvalidEmailException;
}
