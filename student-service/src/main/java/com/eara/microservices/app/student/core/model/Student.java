package com.eara.microservices.app.student.core.model;

import com.eara.microservices.app.student.core.vo.EmailAddress;
import com.eara.microservices.app.student.core.vo.FullName;
import com.eara.microservices.app.student.core.vo.StudentIdentifier;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * This class represents the STUDENTS table.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Entity
@Table(name = "students", schema = "uo_student_service")
@Access(AccessType.FIELD)
@NoArgsConstructor
@EqualsAndHashCode
public class Student {

    @EmbeddedId
    private StudentIdentifier studentId;

    @Embedded
    private FullName fullName;

    @Embedded
    private EmailAddress email;

    @Column(name = "created_at")
    private Date createdAt;

    public Student(StudentIdentifier studentId, FullName fullName, EmailAddress emailAddress) {
        this.studentId = studentId;
        this.fullName = fullName;
        this.email = emailAddress;
    }

    @PrePersist
    public void prePersist() {
        this.createdAt = new Date();
    }

}