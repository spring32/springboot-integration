package com.eara.microservices.app.student.application.exception;

public class InvalidEmailException extends Exception {

    public InvalidEmailException(String message) {
        super(message);
    }
}
