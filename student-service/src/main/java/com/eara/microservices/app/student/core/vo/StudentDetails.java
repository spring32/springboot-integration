package com.eara.microservices.app.student.core.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class StudentDetails {
    private StudentIdentifier studentId;
    private FullName fullName;
    private EmailAddress email;
}
