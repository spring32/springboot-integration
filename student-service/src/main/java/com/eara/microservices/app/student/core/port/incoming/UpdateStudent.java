package com.eara.microservices.app.student.core.port.incoming;

import com.eara.microservices.app.student.application.exception.InvalidEmailException;
import com.eara.microservices.app.student.infrastructure.mapper.StudentDTO;

import java.util.concurrent.CompletableFuture;

public interface UpdateStudent {
    CompletableFuture<StudentDTO> handle(Long studentId, StudentDTO studentDTO) throws InvalidEmailException;
}
