package com.eara.microservices.app.student.core.vo;

import lombok.ToString;
import lombok.Value;
import org.modelmapper.internal.util.Assert;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Value
@ToString
public class FullName implements Serializable, ValueObject, Comparable<FullName> {

    private static final long serialVersionUID = -981919129615409788L;

    @Column(name = "first_name")
    private final String firstName;

    @Column(name = "last_name")
    private final String lastName;

    private FullName() {
        this.firstName = null;
        this.lastName = null;
    }

    public FullName(String firstName, String lastName) {
        Assert.notNull(firstName, "Firstname should not be null");
        Assert.notNull(lastName, "Lastname should not be null");

        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String standardFullName() {
        return new StringBuilder()
                .append(this.firstName)
                .append(" ")
                .append(this.lastName)
                .toString();
    }

    public String reversedFullName() {
        return new StringBuilder()
                .append(this.lastName)
                .append(" ")
                .append(this.firstName)
                .toString();
    }

    @Override
    public int compareTo(FullName other) {
        return 0;
    }
}
