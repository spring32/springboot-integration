package com.eara.microservices.app.student.core.vo;

import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
//@Value
@ToString
public class StudentIdentifier implements Serializable {

    private static final long serialVersionUID = 7535529425843736964L;

    @Column(name = "id")
    private Long value;

    protected StudentIdentifier() {

    }

    public StudentIdentifier(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }

    public String getAsString() {
        String studentIdAsString = Long.toString(this.value);
        return studentIdAsString;
    }
}
